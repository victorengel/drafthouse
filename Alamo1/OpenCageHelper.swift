//
//  OpenCageHelper.swift
//  Alamo1
//
//  Created by Victor Engel on 2/25/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

typealias AsyncCompletionBlock = (
    _ resultDictionary: NSDictionary,
    _ success:Bool,
    _ error:NSError?) -> ()

public typealias ForwardGeocoderCompletionBlock = (
    _ locationDetailsArray: OpenCageResponse,
    _ success:Bool,
    _ error:NSError?) -> ()

class OpenCageHelper: NSObject {
    // Let's create a singleton to manage the requests.
    static let sharedInstance = OpenCageHelper()
    
    func baseURLString() -> String {
        return "https://api.opencagedata.com/geocode/v1/json?key=f4799546974f4d519fedb377f26a9e76"
    }
    
    func downloadDataFromRequest(_ request: URLRequest, completionBlock:@escaping AsyncCompletionBlock) {
        let sessionConfig :URLSessionConfiguration = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfig)
        let task = session.dataTask(with: request, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response, error == nil else {
                completionBlock(NSDictionary(), false, error as NSError?)
                return
            }
            
            var dict :NSDictionary?
            
            do {
                dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary
            } catch _ as NSError {
                
            }
            
            let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            if dict == nil {
                dict = NSDictionary(object: string!, forKey: "data" as NSCopying)
            }
            
            var success = true
            var error :NSError?
            if dict!.object(forKey: "error") != nil {
                success = false
                if let string :String = dict!.object(forKey: "error") as? String {
                    error = NSError(domain: string, code: 0, userInfo: [:])
                }
                else  {
                    error = NSError(domain: "Somethings gone wrong", code: 0, userInfo: [:])
                }
            }
            
            completionBlock(dict!, success, error)
        })
        
        task.resume()
    }
    
    func downloadDataFromURL(_ url: URL, completionBlock:@escaping AsyncCompletionBlock) {
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 10)
        request.httpMethod = "GET"
        self.downloadDataFromRequest(request as URLRequest, completionBlock: completionBlock)
    }
    
    //TODO: Add reverseGeocode logic.
    
    func forwardGeocode(address :String, completionBlock :@escaping ForwardGeocoderCompletionBlock) {
        let urlString = String(format: "%@&language=en&pretty=1&q=%@", baseURLString(), escapedCharacters(address))
        
        let url :URL = URL(string: urlString)!
        downloadDataFromURL(url) { (dict, success, error) in
            let ocResponse :OpenCageResponse = OpenCageResponse()
            
            if success {
                ocResponse.setupWithData(dictionary: dict)
            }
            
            completionBlock(ocResponse, success, error)
        }
    }
    
    /// Converts a string to one with escaped characters, appropriate for adding to the URL string.
    func escapedCharacters(_ string :String) -> String {
        let customAllowedSet =  CharacterSet(charactersIn:"\"#%/<>?@\\^`{|}+, ").inverted
        let escapedString = string.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        
        return escapedString! as String
    }
}
