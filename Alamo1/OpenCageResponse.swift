//
//  OpenCageResponse.swift
//  Alamo1
//
//  Created by Victor Engel on 2/25/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

public class OpenCageResponse: NSObject {
    public var locations : [OpenCageObject] = []
    
    func setupWithData(dictionary :NSDictionary) {
        if let array :NSArray = dictionary.object(forKey: "results") as? NSArray {
            for object in array {
                let dict = object as! NSDictionary
                // Create the objects in the result collection
                let nextLocation = OpenCageObject()
                nextLocation.setupWithDictionary(dict)
                locations.append(nextLocation)
            }
        }
    }
}
