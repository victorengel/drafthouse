//
//  MapViewController.swift
//  Alamo1
//
//  Created by Victor Engel on 2/21/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit
import WebKit

class MapViewController: UIViewController {
    
    private var url : URL? {
        didSet {
            if url != nil, self.mapView != nil, url != self.mapView.url {
                self.setURLForMapView()
            }
        }
    }
    
    @IBOutlet weak var mapView: WKWebView!
    
    private func setURLForMapView() {
        if url != nil {
            self.mapView.load(URLRequest.init(url: url!))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setURLForMapView()
    }
}

extension MapViewController: LocationSelectionDelegate {
    func locationSelected(_ urlString: String) {
        url = URL(string: urlString)
    }
}
