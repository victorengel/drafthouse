//
//  OpenCageObject.swift
//  Alamo1
//
//  Created by Victor Engel on 2/25/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

public class OpenCageObject: NSObject {
    
    //TODO: Add variables and functions for the rest of the API.
    public var formattedAddress: String = ""
    public var latitude: NSNumber!
    public var longitude: NSNumber!
    public var urlString: String = ""
    
    func setupWithDictionary(_ dictionary:NSDictionary) {
        // Grab the nicely formatted address and put it in the public variable.
        if let formattedString :String = dictionary.object(forKey: "formatted") as? String {
            formattedAddress = formattedString
        }
        
        // Grab the coordinates and put them in the respective public variables.
        if let geometryDict :NSDictionary = dictionary.object(forKey: "geometry") as? NSDictionary {
            latitude = NSNumber(value:  geometryDict.object(forKey: "lat") as! Double)
            longitude = NSNumber(value:  geometryDict.object(forKey: "lng") as! Double)
        }
        
        if let annotationsDict : NSDictionary = dictionary.object(forKey: "annotations") as? NSDictionary {
            if let osmDict : NSDictionary = annotationsDict.object(forKey: "OSM") as? NSDictionary {
                urlString = String(osmDict.object(forKey: "url") as! String)
            }
        }
    }
}
