//
//  Throttler.swift
//  Alamo1
//
//  Created by Victor Engel on 2/25/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import Foundation

public class Throttler {
    
    //TODO: consider setting up with qos as an argument.
    private let queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated)
    
    private var job: DispatchWorkItem = DispatchWorkItem(block: {})
    private var previousRun: Date = Date.distantPast
    private var maxInterval: TimeInterval
    
    init(seconds: TimeInterval) {
        self.maxInterval = seconds
    }
    
    func throttle(block: @escaping () -> ()) {
        // If we have a new item, we don't care about pending ones, so cancel them (it, really). Does nothing if nothing pended.
        job.cancel()
        job = DispatchWorkItem(){ [weak self] in
            self?.previousRun = Date()
            block()
        }
        //Set delay to 0 if more than the threshold has already elapsed, else wait for the remaining time.
        var delay = Date.seconds(from: previousRun)
        delay = delay > maxInterval ? 0 : delay
        queue.asyncAfter(deadline: .now() + Double(delay), execute: job)
    }
}
 
//Convenience method to get time elapsed, in seconds, since last request.
private extension Date {
    static func seconds(from referenceDate: Date) -> TimeInterval {
        return Date().timeIntervalSince(referenceDate)
    }
}
