//
//  MasterTableViewController.swift
//  Alamo1
//
//  Created by Victor Engel on 2/21/20.
//  Copyright © 2020 Victor Engel. All rights reserved.
//

import UIKit

var filteredData = [String]()

protocol LocationSelectionDelegate: class {
    func locationSelected(_ urlString: String)
}

class MasterTableViewController: UITableViewController, UISearchResultsUpdating {
    
    let tableData = ["Enter a place"]
    var resultSearchController = UISearchController()

    // Set up a private variable to hold requests that are pending. We
    private var pendingRequest : String = ""
    private var throttler : Throttler?

    var resultLocations = [OpenCageObject]()
    var urlStringForSelectedRow : String?
    
    // Because of the asynchronous nature of the requests, despite the throttling, results could come back out of order. To deal with that we store a sequential request number and use the results only if the request number is the same. If it's not the same, then a later request must have finished earlier.
    var requestNumber = 0
    
    // Add a reference to the detail VC to keep it from being released. Hopefully this clears the warning message ProcessAssertion::processAssertionWasInvalidated()
    var detailVC : UIViewController?
    weak var delegate : LocationSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup the throttler
        throttler = Throttler(seconds: 1.0)
        self.definesPresentationContext = true
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.obscuresBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            tableView.tableHeaderView = controller.searchBar
            return controller
        })()
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    /* Might be useful to use this for different types of results. Comment out for now since we'll use only one section to start.
     override func numberOfSections(in tableView: UITableView) -> Int {
     return 0
     }
     */
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  (resultSearchController.isActive) {
            return filteredData.count
        } else {
            return tableData.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if resultSearchController.isActive {
            cell.textLabel?.text = filteredData[indexPath.row]
        } else {
            cell.textLabel?.text = tableData[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.urlStringForSelectedRow = self.resultLocations[indexPath.row].urlString
        self.delegate?.locationSelected(self.urlStringForSelectedRow ?? "")
        if let detailViewController = delegate as? MapViewController {
          splitViewController?.showDetailViewController(detailViewController, sender: nil)
        }    }
    
    // MARK: - Navigation
    
    func updateSearchResults(for searchController: UISearchController) {
        throttler?.throttle {
            self.requestNumber += 1
            let thisRequestNumber = self.requestNumber
            filteredData.removeAll(keepingCapacity: false)
            DispatchQueue.main.async {
                // Normally, I would consider updating the network activity indicator here, but in this case, it seems obvious that the user will know that there is activity because he/she is requesting it directly. So that is being omitted here.
                OpenCageHelper.sharedInstance.forwardGeocode(address: searchController.searchBar.text!) { (response, success, error) in
                    if success {
                        DispatchQueue.main.async { [weak self] in
                            // If self no longer exists, there is nothing to do.
                            guard let self = self else { return }
                            
                            // If the reequest number doesn't match, we're looking at an obsolete request. A more current one already has been dispatched and may have already completed. If we continue anyway, we could be updating the results to an old query, and the user wonders why the latest request was not processed.
                            if thisRequestNumber != self.requestNumber { return }
                            self.resultLocations = response.locations
                            var array = [String]()
                            for item in response.locations {
                                array.append(item.formattedAddress)
                            }
                            filteredData = array
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}
